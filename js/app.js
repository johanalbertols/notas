let notas = [];

const btnAgregar = document.querySelector('.btn-agregar');


btnAgregar.addEventListener('click', ()=>{ //agregando un evento al boton
	agregar()
})

function agregar(){

	const textoInput = document.querySelector('.form-control');
	if(textoInput.value == ""){
		alert("no se pueden agregar notas vacias");
	}
	else {
		notas.push(textoInput.value);
		const n = notas.length - 1;
		
		const template = document.querySelector('.template-notas').content;
		const contenedorNotas = document.querySelector('.contenedor-notas');

		template.querySelector('.parrafo').textContent = notas[n];
		template.querySelector('.btn-eliminar').setAttribute('id', n);

		const clone = template.cloneNode(true);

		clone.querySelector('.nueva-fila').className += ' '+n

		contenedorNotas.appendChild(clone);

		textoInput.value = "";
	}
}

function eliminar() {

	const id = event.target.id;
	const elemento = notas.indexOf(id);

	notas.splice(elemento,1);

	const div = document.getElementsByClassName(id)[0];

	div.parentNode.removeChild(div)

}